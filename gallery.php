<?php
error_reporting(E_NONE);
ini_set ( 'register_globals', 0 );

// IZSImage - Isaac Zimmitti Schlueter's Image Gallery
// Pagination, image styling and file presentation by Amit Gurdasani ©2008

// Licensed under a Creative Commons (Share Alike-Attribution) License
// You can share this, and make derivative works, but your changes must be clearly
// documented so that your bugs don't reflect badly on me, and any derivative works
// must be released under the same CC license.

// You MAY NOT remove the link to my site, or just change the title and call it a
// "derivative work" while all the functionality is exactly the same.  Any 
// derivative works must clearly mention that they are based upon this script.
// License: http://creativecommons.org/licenses/by-sa/2.5/

// Copyright 2005 Isaac Z. Schlueter, Some Rights Reserved.

/** Installation:
0. Requirements:
The GD image library for PHP (for auto-generation of thumbnails)
mod_rewrite
Supported Filetypes: .cur, .bmp, .ico, .jpg (or .jpeg), .gif, .png
Files that don't match any of these types will be shown as links, but no thumbnail will be created.

1. Drop this file in your "images" folder.
2. chmod your "images" folder to 777
3. Put images in your images folder.  You can also create and use subdirectories, but
  DO NOT use a subdirectory called "thumbnails"! This is for auto-generated thumbnails ONLY!
4. Put this in a .htaccess file in the images folder:

RewriteEngine On
# replace this with your actual "images" folder, relative to the website.
# i.e., if your images folder in http://your.site.com/images/gallery/ then
# set this to /images/gallery/
RewriteBase /images/
RewriteRule \.imgpasswd - [F]
RewriteCond %{REQUEST_FILENAME} -d [NC]
# change this to the actual (public) path to gallery.php.
RewriteCond %{QUERY_STRING} ^(.*)$
RewriteRule ^(.*)$ /images/gallery.php?dir=$1&%1 [QSA,L]


5. Modify the variables below according to your system.

To password-protect a directory:
1. Create a file called .imgpasswd (no prefix!)
2. Open it up in your favorite text editor, and add a line like this:
username:password
3. Put it in the directory that you want to protect.  All subdirectories
will be password-protected.  If you want to un-protect a subdirectory, then
create another .imgpasswd file, and leave the contents blank, and upload that
file into the subdirectory.

  **/


// ------------------------------- BEGIN EDITING --------------------------------- //

// thumbnail width (min, max) in pixels.
$_THUMB_WIDTH = array(5, 150);

 // thumbnail height (min, max) in pixels.
$_THUMB_HEIGHT = array(5, 150);

// the prefix for thumbnail files
$_THUMB_PREFIX = 'thumb_';

// domain for setting password cookies.
$_COOKIE_DOMAIN = '.gurdasani.com';

// number of seconds to store cookies - 60*60*24 = 1 day.
$_COOKIE_TIME = 60*60*24;

// password to clear out thumbnail files. (images/folder/?clearthumbs=$_CLEARTHUMBS_PASSWORD)
// YOU MUST CHANGE THIS BEFORE THE SCRIPT WILL WORK!
$_CLEARTHUMBS_PASSWORD = 'your_password_here';

$_CONTACT_URL = ''; // the link to your "contact me" form.
// $_CONTACT_URL = 'mailto:me@mydomain.com'; // another option: enter your email address.  NOT SECURE!
// $_CONTACT_URL = ''; // another option: leave blank for no contact information being displayed.

$_PAGINATION_FACTOR = 32;

// --------------- STOP EDITING UNLESS YOU KNOW WHAT YOURE DOING!! --------------- //




/*
*------------------------------------------------------------
*                   ICO Image functions
*------------------------------------------------------------
*                      By JPEXS
*/

// Nabbed from: http://jpexs.wz.cz/?page=php&Language=eng

define("TRUE_COLOR", 16777216);
define("XP_COLOR", 4294967296);
define("MAX_COLOR", -2);
define("MAX_SIZE", -2);


/*
*------------------------------------------------------------
*                    ImageCreateFromIco
*------------------------------------------------------------
*            - Reads image from a ICO file
*
*         Parameters:  $filename - Target ico file to load
*                 $icoColorCount - Icon color count (For multiple icons ico file)
*                                - 2,16,256, TRUE_COLOR or XP_COLOR
*                       $icoSize - Icon width       (For multiple icons ico file)
*            Returns: Image ID
*/

function ImageCreateFromIco($filename,$icoColorCount=16,$icoSize=16)
{
$Ikona=GetIconsInfo($filename);

$IconID=-1;

$ColMax=-1;
$SizeMax=-1;

for($p=0;$p<count($Ikona);$p++)
{
$Ikona[$p]["NumberOfColors"]=pow(2,$Ikona[$p]["Info"]["BitsPerPixel"]);
};

for($p=0;$p<count($Ikona);$p++)
{

if(($ColMax==-1)or($Ikona[$p]["NumberOfColors"]>$Ikona[$ColMax]["NumberOfColors"]))
if(($icoSize==$Ikona[$p]["Width"])or($icoSize==-2))
 {
  $ColMax=$p;
 };

if(($SizeMax==-1)or($Ikona[$p]["Width"]>$Ikona[$SizeMax]["Width"]))
if(($icoColorCount==$Ikona[$p]["NumberOfColors"])or($icoColorCount==-2))
 {
   $SizeMax=$p;
 };

if($Ikona[$p]["NumberOfColors"]==$icoColorCount)
if($Ikona[$p]["Width"]==$icoSize)
 {

 $IconID=$p;
 };
};

if($icoSize==-2) $IconID=$SizeMax;
if($icoColorCount==-2) $IconID=$ColMax;

$ColName=$icoColorCount;

if($icoSize==-2) $icoSize="Max";
if($ColName==16777216) $ColName="True";
if($ColName==4294967296) $ColName="XP";
if($ColName==-2) $ColName="Max";
if($IconID==-1) die("Icon with $ColName colors and $icoSize x $icoSize size doesn't exist in this file!");

ReadIcon($filename,$IconID,$Ikona);

 $biBitCount=$Ikona[$IconID]["Info"]["BitsPerPixel"];

  if($Ikona[$IconID]["Info"]["BitsPerPixel"]==0)
  {
  $Ikona[$IconID]["Info"]["BitsPerPixel"]=24;
  };

 $biBitCount=$Ikona[$IconID]["Info"]["BitsPerPixel"];
 if($biBitCount==0) $biBitCount=1;

$Ikona[$IconID]["BitCount"]=$Ikona[$IconID]["Info"]["BitsPerPixel"];

if($Ikona[$IconID]["BitCount"]>=24)
{
$img=imagecreatetruecolor($Ikona[$IconID]["Width"],$Ikona[$IconID]["Height"]);

for($y=0;$y<$Ikona[$IconID]["Height"];$y++)
for($x=0;$x<$Ikona[$IconID]["Width"];$x++)
 {
 $R=$Ikona[$IconID]["Data"][$x][$y]["r"];
 $G=$Ikona[$IconID]["Data"][$x][$y]["g"];
 $B=$Ikona[$IconID]["Data"][$x][$y]["b"];
 $Alpha=round($Ikona[$IconID]["Data"][$x][$y]["alpha"]/2);

 if($Ikona[$IconID]["BitCount"]==32)
 {
 $color=imagecolorexactalpha($img,$R,$G,$B,$Alpha);
 if($color==-1) $color=imagecolorallocatealpha($img,$R,$G,$B,$Alpha);
 }
 else
 {
 $color=imagecolorexact($img,$R,$G,$B);
 if($color==-1) $color=imagecolorallocate($img,$R,$G,$B);
 };

 imagesetpixel($img,$x,$y,$color);

 };

}
else
{
$img=imagecreate($Ikona[$IconID]["Width"],$Ikona[$IconID]["Height"]);
for($p=0;$p<count($Ikona[$IconID]["Paleta"]);$p++)
 $Paleta[$p]=imagecolorallocate($img,$Ikona[$IconID]["Paleta"][$p]["r"],$Ikona[$IconID]["Paleta"][$p]["g"],$Ikona[$IconID]["Paleta"][$p]["b"]);

for($y=0;$y<$Ikona[$IconID]["Height"];$y++)
for($x=0;$x<$Ikona[$IconID]["Width"];$x++)
 {
 imagesetpixel($img,$x,$y,$Paleta[$Ikona[$IconID]["Data"][$x][$y]]);
 };
};

for($y=0;$y<$Ikona[$IconID]["Height"];$y++)
for($x=0;$x<$Ikona[$IconID]["Width"];$x++)
 if($Ikona[$IconID]["Maska"][$x][$y]==1)
  {
   $IsTransparent=true;
   break;
  };
if($Ikona[$IconID]["BitCount"]==32)
{
 imagealphablending($img, FALSE);
 if(function_exists("imagesavealpha"))
  imagesavealpha($img,true);
};

 if($IsTransparent)
 {
  if(($Ikona[$IconID]["BitCount"]>=24)or(imagecolorstotal($img)>=256))
   {
   $img2=imagecreatetruecolor(imagesx($img),imagesy($img));
   imagecopy($img2,$img,0,0,0,0,imagesx($img),imagesy($img));
   imagedestroy($img);
   $img=$img2;
   imagetruecolortopalette($img,true,255);

   };
    $Pruhledna=imagecolorallocate($img,0,0,0);
    for($y=0;$y<$Ikona[$IconID]["Height"];$y++)
     for($x=0;$x<$Ikona[$IconID]["Width"];$x++)
      if($Ikona[$IconID]["Maska"][$x][$y]==1)
       {
        imagesetpixel($img,$x,$y,$Pruhledna);
       };
  imagecolortransparent($img,$Pruhledna);
 };

return $img;

};

function ReadIcon($filename,$id,&$Ikona)
{
global $CurrentBit;

$f=fopen($filename,"rb");

fseek($f,6+$id*16);
  $Width=freadbyte($f);
  $Height=freadbyte($f);
fseek($f,6+$id*16+12);
$OffSet=freaddword($f);
fseek($f,$OffSet);

$p=$id;

  $Ikona[$p]["Info"]["HeaderSize"]=freadlngint($f);
  $Ikona[$p]["Info"]["ImageWidth"]=freadlngint($f);
  $Ikona[$p]["Info"]["ImageHeight"]=freadlngint($f);
  $Ikona[$p]["Info"]["NumberOfImagePlanes"]=freadword($f);
  $Ikona[$p]["Info"]["BitsPerPixel"]=freadword($f);
  $Ikona[$p]["Info"]["CompressionMethod"]=freadlngint($f);
  $Ikona[$p]["Info"]["SizeOfBitmap"]=freadlngint($f);
  $Ikona[$p]["Info"]["HorzResolution"]=freadlngint($f);
  $Ikona[$p]["Info"]["VertResolution"]=freadlngint($f);
  $Ikona[$p]["Info"]["NumColorUsed"]=freadlngint($f);
  $Ikona[$p]["Info"]["NumSignificantColors"]=freadlngint($f);

 $biBitCount=$Ikona[$p]["Info"]["BitsPerPixel"];

 if($Ikona[$p]["Info"]["BitsPerPixel"]<=8)
  {

 $barev=pow(2,$biBitCount);

  for($b=0;$b<$barev;$b++)
    {
    $Ikona[$p]["Paleta"][$b]["b"]=freadbyte($f);
    $Ikona[$p]["Paleta"][$b]["g"]=freadbyte($f);
    $Ikona[$p]["Paleta"][$b]["r"]=freadbyte($f);
    freadbyte($f);
    };

$Zbytek=(4-ceil(($Width/(8/$biBitCount)))%4)%4;

for($y=$Height-1;$y>=0;$y--)
    {
     $CurrentBit=0;
     for($x=0;$x<$Width;$x++)
      {
         $C=freadbits($f,$biBitCount);
         $Ikona[$p]["Data"][$x][$y]=$C;
      };

    if($CurrentBit!=0) {freadbyte($f);};
    for($g=0;$g<$Zbytek;$g++)
     freadbyte($f);
     };

}
elseif($biBitCount==24)
{
 $Zbytek=$Width%4;

   for($y=$Height-1;$y>=0;$y--)
    {
     for($x=0;$x<$Width;$x++)
      {
       $B=freadbyte($f);
       $G=freadbyte($f);
       $R=freadbyte($f);
       $Ikona[$p]["Data"][$x][$y]["r"]=$R;
       $Ikona[$p]["Data"][$x][$y]["g"]=$G;
       $Ikona[$p]["Data"][$x][$y]["b"]=$B;
      }
    for($z=0;$z<$Zbytek;$z++)
     freadbyte($f);
   };
}
elseif($biBitCount==32)
{
 $Zbytek=$Width%4;

   for($y=$Height-1;$y>=0;$y--)
    {
     for($x=0;$x<$Width;$x++)
      {
       $B=freadbyte($f);
       $G=freadbyte($f);
       $R=freadbyte($f);
       $Alpha=freadbyte($f);
       $Ikona[$p]["Data"][$x][$y]["r"]=$R;
       $Ikona[$p]["Data"][$x][$y]["g"]=$G;
       $Ikona[$p]["Data"][$x][$y]["b"]=$B;
       $Ikona[$p]["Data"][$x][$y]["alpha"]=$Alpha;
      }
    for($z=0;$z<$Zbytek;$z++)
     freadbyte($f);
   };
};

//Maska
$Zbytek=(4-ceil(($Width/(8)))%4)%4;
for($y=$Height-1;$y>=0;$y--)
    {
     $CurrentBit=0;
     for($x=0;$x<$Width;$x++)
      {
         $C=freadbits($f,1);
         $Ikona[$p]["Maska"][$x][$y]=$C;
      };
    if($CurrentBit!=0) {freadbyte($f);};
    for($g=0;$g<$Zbytek;$g++)
     freadbyte($f);
     };
//--------------

fclose($f);

};

function GetIconsInfo($filename)
{
global $CurrentBit;

$f=fopen($filename,"rb");

$Reserved=freadword($f);
$Type=freadword($f);
$Count=freadword($f);
for($p=0;$p<$Count;$p++)
 {
  $Ikona[$p]["Width"]=freadbyte($f);
  $Ikona[$p]["Height"]=freadbyte($f);
  $Ikona[$p]["ColorCount"]=freadword($f);
 if($Ikona[$p]["ColorCount"]==0) $Ikona[$p]["ColorCount"]=256;
  $Ikona[$p]["Planes"]=freadword($f);
  $Ikona[$p]["BitCount"]=freadword($f);
  $Ikona[$p]["BytesInRes"]=freaddword($f);
  $Ikona[$p]["ImageOffset"]=freaddword($f);
 };

for($p=0;$p<$Count;$p++)
 {
  fseek($f,$Ikona[$p]["ImageOffset"]+14);
  $Ikona[$p]["Info"]["BitsPerPixel"]=freadword($f);
 };

fclose($f);
return $Ikona;
};

/*
*------------------------------------------------------------
*                       ImageIco
*------------------------------------------------------------
*                 - Returns ICO file
*
*         Parameters:       $img - Target Image (Can be array of images)
*                      $filename - Target ico file to save
*
*
* Note: For returning icons to Browser, you have to set header:
*
*             header("Content-type: image/x-icon");
*
*/

function ImageIco($Images/*image or image array*/,$filename="")
{

if(is_array($Images))
{
$ImageCount=count($Images);
$Image=$Images;
}
else
{
$Image[0]=$Images;
$ImageCount=1;
};

$WriteToFile=false;

if($filename!="")
{
$WriteToFile=true;
};

$ret="";

$ret.=inttoword(0); //PASSWORD
$ret.=inttoword(1); //SOURCE
$ret.=inttoword($ImageCount); //ICONCOUNT

for($q=0;$q<$ImageCount;$q++)
{
$img=$Image[$q];

$Width=imagesx($img);
$Height=imagesy($img);

$ColorCount=imagecolorstotal($img);

$Transparent=imagecolortransparent($img);
$IsTransparent=$Transparent!=-1;

if($IsTransparent) $ColorCount--;

if($ColorCount==0) {$ColorCount=0; $BitCount=24;};
if(($ColorCount>0)and($ColorCount<=2)) {$ColorCount=2; $BitCount=1;};
if(($ColorCount>2)and($ColorCount<=16)) { $ColorCount=16; $BitCount=4;};
if(($ColorCount>16)and($ColorCount<=256)) { $ColorCount=0; $BitCount=8;};

//ICONINFO:
$ret.=inttobyte($Width);//
$ret.=inttobyte($Height);//
$ret.=inttobyte($ColorCount);//
$ret.=inttobyte(0);//RESERVED

$Planes=0;
if($BitCount>=8) $Planes=1;

$ret.=inttoword($f,$Planes);//PLANES
if($BitCount>=8) $WBitCount=$BitCount;
if($BitCount==4) $WBitCount=0;
if($BitCount==1) $WBitCount=0;
$ret.=inttoword($WBitCount);//BITS

$Zbytek=(4-($Width/(8/$BitCount))%4)%4;
$ZbytekMask=(4-($Width/8)%4)%4;

$PalSize=0;

$Size=40+($Width/(8/$BitCount)+$Zbytek)*$Height+(($Width/8+$ZbytekMask) * $Height);
if($BitCount<24)
 $Size+=pow(2,$BitCount)*4;
$IconId=1;
$ret.=inttodword($Size); //SIZE
$OffSet=6+16*$ImageCount+$FullSize;
$ret.=inttodword(6+16*$ImageCount+$FullSize);//OFFSET
$FullSize+=$Size;
//-------------

};

for($q=0;$q<$ImageCount;$q++)
{
$img=$Image[$q];
$Width=imagesx($img);
$Height=imagesy($img);
$ColorCount=imagecolorstotal($img);

$Transparent=imagecolortransparent($img);
$IsTransparent=$Transparent!=-1;

if($IsTransparent) $ColorCount--;
if($ColorCount==0) {$ColorCount=0; $BitCount=24;};
if(($ColorCount>0)and($ColorCount<=2)) {$ColorCount=2; $BitCount=1;};
if(($ColorCount>2)and($ColorCount<=16)) { $ColorCount=16; $BitCount=4;};
if(($ColorCount>16)and($ColorCount<=256)) { $ColorCount=0; $BitCount=8;};

//ICONS
$ret.=inttodword(40);//HEADSIZE
$ret.=inttodword($Width);//
$ret.=inttodword(2*$Height);//
$ret.=inttoword(1); //PLANES
$ret.=inttoword($BitCount);   //
$ret.=inttodword(0);//Compress method

$ZbytekMask=($Width/8)%4;

$Zbytek=($Width/(8/$BitCount))%4;
$Size=($Width/(8/$BitCount)+$Zbytek)*$Height+(($Width/8+$ZbytekMask) * $Height);

$ret.=inttodword($Size);//SIZE

$ret.=inttodword(0);//HPIXEL_M
$ret.=inttodword(0);//V_PIXEL_M
$ret.=inttodword($ColorCount); //UCOLORS
$ret.=inttodword(0); //DCOLORS
//---------------

$CC=$ColorCount;
if($CC==0) $CC=256;

if($BitCount<24)
{
 $ColorTotal=imagecolorstotal($img);
 if($IsTransparent) $ColorTotal--;

 for($p=0;$p<$ColorTotal;$p++)
  {
   $color=imagecolorsforindex($img,$p);
   $ret.=inttobyte($color["blue"]);
   $ret.=inttobyte($color["green"]);
   $ret.=inttobyte($color["red"]);
   $ret.=inttobyte(0); //RESERVED
  };

 $CT=$ColorTotal;
 for($p=$ColorTotal;$p<$CC;$p++)
  {
   $ret.=inttobyte(0);
   $ret.=inttobyte(0);
   $ret.=inttobyte(0);
   $ret.=inttobyte(0); //RESERVED
  };
};

if($BitCount<=8)
{

 for($y=$Height-1;$y>=0;$y--)
 {
  $bWrite="";
  for($x=0;$x<$Width;$x++)
   {
   $color=imagecolorat($img,$x,$y);
   if($color==$Transparent)
    $color=imagecolorexact($img,0,0,0);
   if($color==-1) $color=0;
   if($color>pow(2,$BitCount)-1) $color=0;

   $bWrite.=decbinx($color,$BitCount);
   if(strlen($bWrite)==8)
    {
     $ret.=inttobyte(bindec($bWrite));
     $bWrite="";
    };
   };

  if((strlen($bWrite)<8)and(strlen($bWrite)!=0))
    {
     $sl=strlen($bWrite);
     for($t=0;$t<8-$sl;$t++)
      $sl.="0";
     $ret.=inttobyte(bindec($bWrite));
    };
  for($z=0;$z<$Zbytek;$z++)
   $ret.=inttobyte(0);
 };
};

if($BitCount>=24)
{
 for($y=$Height-1;$y>=0;$y--)
 {
  for($x=0;$x<$Width;$x++)
   {
   $color=imagecolorsforindex($img,imagecolorat($img,$x,$y));
   $ret.=inttobyte($color["blue"]);
   $ret.=inttobyte($color["green"]);
   $ret.=inttobyte($color["red"]);
   if($BitCount==32)
    $ret.=inttobyte(0);//Alpha for XP_COLORS
   };
  for($z=0;$z<$Zbytek;$z++)
   $ret.=inttobyte(0);
 };
};

//MASK

 for($y=$Height-1;$y>=0;$y--)
 {
  $byteCount=0;
  $bOut="";
  for($x=0;$x<$Width;$x++)
   {
    if(($Transparent!=-1)and(imagecolorat($img,$x,$y)==$Transparent))
     {
      $bOut.="1";
     }
     else
     {
      $bOut.="0";
     };
   };
  for($p=0;$p<strlen($bOut);$p+=8)
  {
   $byte=bindec(substr($bOut,$p,8));
   $byteCount++;
   $ret.=inttobyte($byte);
  // echo dechex($byte)." ";
  };
 $Zbytek=$byteCount%4;
  for($z=0;$z<$Zbytek;$z++)
   {
   $ret.=inttobyte(0xff);
  // echo "FF ";
   };
 };

//------------------

};//q

if($WriteToFile)
{
 $f=fopen($filename,"w");
 fwrite($f,$ret);
 fclose($f);
}
else
{
 echo $ret;
};

};

/*
* Helping functions:
*-------------------------
*
* inttobyte($n) - returns chr(n)
* inttodword($n) - returns dword (n)
* inttoword($n) - returns word(n)
* freadbyte($file) - reads 1 byte from $file
* freadword($file) - reads 2 bytes (1 word) from $file
* freaddword($file) - reads 4 bytes (1 dword) from $file
* freadlngint($file) - same as freaddword($file)
* decbin8($d) - returns binary string of d zero filled to 8
* RetBits($byte,$start,$len) - returns bits $start->$start+$len from $byte
* freadbits($file,$count) - reads next $count bits from $file
*/

function decbin8($d)
{
return decbinx($d,8);
};

function decbinx($d,$n)
{
$bin=decbin($d);
$sbin=strlen($bin);
for($j=0;$j<$n-$sbin;$j++)
 $bin="0$bin";
return $bin;
};

function RetBits($byte,$start,$len)
{
$bin=decbin8($byte);
$r=bindec(substr($bin,$start,$len));
return $r;

};

$CurrentBit=0;
function freadbits($f,$count)
{
 global $CurrentBit,$SMode;
 $Byte=freadbyte($f);
 $LastCBit=$CurrentBit;
 $CurrentBit+=$count;
 if($CurrentBit==8)
  {
   $CurrentBit=0;
  }
 else
  {
   fseek($f,ftell($f)-1);
  };
 return RetBits($Byte,$LastCBit,$count);
};

function freadbyte($f)
{
 return ord(fread($f,1));
};

function freadword($f)
{
 $b1=freadbyte($f);
 $b2=freadbyte($f);
 return $b2*256+$b1;
};

function freadlngint($f)
{
return freaddword($f);
};

function freaddword($f)
{
 $b1=freadword($f);
 $b2=freadword($f);
 return $b2*65536+$b1;
};

function inttobyte($n)
{
return chr($n);
};

function inttodword($n)
{
return chr($n & 255).chr(($n >> 8) & 255).chr(($n >> 16) & 255).chr(($n >> 24) & 255);
};

function inttoword($n)
 {
 return chr($n & 255).chr(($n >> 8) & 255);
};
/*
*------------------------------------------------------------
*                   BMP Image functions
*------------------------------------------------------------
*                      By JPEXS
*/

// Nabbed from: http://jpexs.wz.cz/?page=php&Language=eng

/*
*------------------------------------------------------------
*                    ImageBMP
*------------------------------------------------------------
*            - Creates new BMP file
*
*         Parameters:  $img - Target image
*                      $file - Target file to store
*                            - if not specified, bmp is returned
*
*           Returns: if $file specified - true if OK
                     if $file not specified - image data
*/
function imagebmp($img,$file="",$RLE=0)
{

$ColorCount=imagecolorstotal($img);

$Transparent=imagecolortransparent($img);
$IsTransparent=$Transparent!=-1;

if($IsTransparent) $ColorCount--;

if($ColorCount==0) {$ColorCount=0; $BitCount=24;};
if(($ColorCount>0)and($ColorCount<=2)) {$ColorCount=2; $BitCount=1;};
if(($ColorCount>2)and($ColorCount<=16)) { $ColorCount=16; $BitCount=4;};
if(($ColorCount>16)and($ColorCount<=256)) { $ColorCount=0; $BitCount=8;};

                $Width=imagesx($img);
                $Height=imagesy($img);

                $Zbytek=(4-($Width/(8/$BitCount))%4)%4;

                if($BitCount<24) $palsize=pow(2,$BitCount)*4;

                $size=(floor($Width/(8/$BitCount))+$Zbytek)*$Height+54;
                $size+=$palsize;
                $offset=54+$palsize;

                // Bitmap File Header
                $ret = 'BM';                        // header (2b)
                $ret .= int_to_dword($size);        // size of file (4b)
                $ret .= int_to_dword(0);        // reserved (4b)
                $ret .= int_to_dword($offset);        // byte location in the file which is first byte of IMAGE (4b)
                // Bitmap Info Header
                $ret .= int_to_dword(40);        // Size of BITMAPINFOHEADER (4b)
                $ret .= int_to_dword($Width);        // width of bitmap (4b)
                $ret .= int_to_dword($Height);        // height of bitmap (4b)
                $ret .= int_to_word(1);        // biPlanes = 1 (2b)
                $ret .= int_to_word($BitCount);        // biBitCount = {1 (mono) or 4 (16 clr ) or 8 (256 clr) or 24 (16 Mil)} (2b)
                $ret .= int_to_dword($RLE);        // RLE COMPRESSION (4b)
                $ret .= int_to_dword(0);        // width x height (4b)
                $ret .= int_to_dword(0);        // biXPelsPerMeter (4b)
                $ret .= int_to_dword(0);        // biYPelsPerMeter (4b)
                $ret .= int_to_dword(0);        // Number of palettes used (4b)
                $ret .= int_to_dword(0);        // Number of important colour (4b)
                // image data

                $CC=$ColorCount;
                $sl1=strlen($ret);
                if($CC==0) $CC=256;
                if($BitCount<24)
                   {
                    $ColorTotal=imagecolorstotal($img);
                     if($IsTransparent) $ColorTotal--;

                     for($p=0;$p<$ColorTotal;$p++)
                     {
                      $color=imagecolorsforindex($img,$p);
                       $ret.=inttobyte($color["blue"]);
                       $ret.=inttobyte($color["green"]);
                       $ret.=inttobyte($color["red"]);
                       $ret.=inttobyte(0); //RESERVED
                     };

                    $CT=$ColorTotal;
                  for($p=$ColorTotal;$p<$CC;$p++)
                       {
                      $ret.=inttobyte(0);
                      $ret.=inttobyte(0);
                      $ret.=inttobyte(0);
                      $ret.=inttobyte(0); //RESERVED
                     };
                   };

if($BitCount<=8)
{

 for($y=$Height-1;$y>=0;$y--)
 {
  $bWrite="";
  for($x=0;$x<$Width;$x++)
   {
   $color=imagecolorat($img,$x,$y);
   $bWrite.=decbinx($color,$BitCount);
   if(strlen($bWrite)==8)
    {
     $retd.=inttobyte(bindec($bWrite));
     $bWrite="";
    };
   };

  if((strlen($bWrite)<8)and(strlen($bWrite)!=0))
    {
     $sl=strlen($bWrite);
     for($t=0;$t<8-$sl;$t++)
      $sl.="0";
     $retd.=inttobyte(bindec($bWrite));
    };
 for($z=0;$z<$Zbytek;$z++)
   $retd.=inttobyte(0);
 };
};

if(($RLE==1)and($BitCount==8))
{
 for($t=0;$t<strlen($retd);$t+=4)
  {
   if($t!=0)
   if(($t)%$Width==0)
    $ret.=chr(0).chr(0);

   if(($t+5)%$Width==0)
   {
     $ret.=chr(0).chr(5).substr($retd,$t,5).chr(0);
     $t+=1;
   }
   if(($t+6)%$Width==0)
    {
     $ret.=chr(0).chr(6).substr($retd,$t,6);
     $t+=2;
    }
    else
    {
     $ret.=chr(0).chr(4).substr($retd,$t,4);
    };
  };
  $ret.=chr(0).chr(1);
}
else
{
$ret.=$retd;
};

                if($BitCount==24)
                {
                for($z=0;$z<$Zbytek;$z++)
                 $Dopl.=chr(0);

                for($y=$Height-1;$y>=0;$y--)
                 {
                 for($x=0;$x<$Width;$x++)
                        {
                           $color=imagecolorsforindex($img,ImageColorAt($img,$x,$y));
                           $ret.=chr($color["blue"]).chr($color["green"]).chr($color["red"]);
                        }
                 $ret.=$Dopl;
                 };

                 };

  if($file!="")
   {
    $r=($f=fopen($file,"w"));
    $r=$r and fwrite($f,$ret);
    $r=$r and fclose($f);
    return $r;
   }
  else
  {
   echo $ret;
  };
};

/*
*------------------------------------------------------------
*                    ImageCreateFromBmp
*------------------------------------------------------------
*            - Reads image from a BMP file
*
*         Parameters:  $file - Target file to load
*
*            Returns: Image ID
*/

function imagecreatefrombmp($file)
{
global  $CurrentBit, $echoMode;

$f=fopen($file,"r");
$Header=fread($f,2);

if($Header=="BM")
{
 $Size=freaddword($f);
 $Reserved1=freadword($f);
 $Reserved2=freadword($f);
 $FirstByteOfImage=freaddword($f);

 $SizeBITMAPINFOHEADER=freaddword($f);
 $Width=freaddword($f);
 $Height=freaddword($f);
 $biPlanes=freadword($f);
 $biBitCount=freadword($f);
 $RLECompression=freaddword($f);
 $WidthxHeight=freaddword($f);
 $biXPelsPerMeter=freaddword($f);
 $biYPelsPerMeter=freaddword($f);
 $NumberOfPalettesUsed=freaddword($f);
 $NumberOfImportantColors=freaddword($f);

if($biBitCount<24)
 {
  $img=imagecreate($Width,$Height);
  $Colors=pow(2,$biBitCount);
  for($p=0;$p<$Colors;$p++)
   {
    $B=freadbyte($f);
    $G=freadbyte($f);
    $R=freadbyte($f);
    $Reserved=freadbyte($f);
    $Palette[]=imagecolorallocate($img,$R,$G,$B);
   };

if($RLECompression==0)
{
   $Zbytek=(4-ceil(($Width/(8/$biBitCount)))%4)%4;

for($y=$Height-1;$y>=0;$y--)
    {
     $CurrentBit=0;
     for($x=0;$x<$Width;$x++)
      {
         $C=freadbits($f,$biBitCount);
       imagesetpixel($img,$x,$y,$Palette[$C]);
      };
    if($CurrentBit!=0) {freadbyte($f);};
    for($g=0;$g<$Zbytek;$g++)
     freadbyte($f);
     };

 };
};

if($RLECompression==1) //$BI_RLE8
{
$y=$Height;

$pocetb=0;

while(true)
{
$y--;
$prefix=freadbyte($f);
$suffix=freadbyte($f);
$pocetb+=2;

$echoit=false;

if($echoit)echo "Prefix: $prefix Suffix: $suffix<BR>";
if(($prefix==0)and($suffix==1)) break;
if(feof($f)) break;

while(!(($prefix==0)and($suffix==0)))
{
 if($prefix==0)
  {
   $pocet=$suffix;
   $Data.=fread($f,$pocet);
   $pocetb+=$pocet;
   if($pocetb%2==1) {freadbyte($f); $pocetb++;};
  };
 if($prefix>0)
  {
   $pocet=$prefix;
   for($r=0;$r<$pocet;$r++)
    $Data.=chr($suffix);
  };
 $prefix=freadbyte($f);
 $suffix=freadbyte($f);
 $pocetb+=2;
 if($echoit) echo "Prefix: $prefix Suffix: $suffix<BR>";
};

for($x=0;$x<strlen($Data);$x++)
 {
  imagesetpixel($img,$x,$y,$Palette[ord($Data[$x])]);
 };
$Data="";

};

};

if($RLECompression==2) //$BI_RLE4
{
$y=$Height;
$pocetb=0;

/*while(!feof($f))
 echo freadbyte($f)."_".freadbyte($f)."<BR>";*/
while(true)
{
//break;
$y--;
$prefix=freadbyte($f);
$suffix=freadbyte($f);
$pocetb+=2;

$echoit=false;

if($echoit)echo "Prefix: $prefix Suffix: $suffix<BR>";
if(($prefix==0)and($suffix==1)) break;
if(feof($f)) break;

while(!(($prefix==0)and($suffix==0)))
{
 if($prefix==0)
  {
   $pocet=$suffix;

   $CurrentBit=0;
   for($h=0;$h<$pocet;$h++)
    $Data.=chr(freadbits($f,4));
   if($CurrentBit!=0) freadbits($f,4);
   $pocetb+=ceil(($pocet/2));
   if($pocetb%2==1) {freadbyte($f); $pocetb++;};
  };
 if($prefix>0)
  {
   $pocet=$prefix;
   $i=0;
   for($r=0;$r<$pocet;$r++)
    {
    if($i%2==0)
     {
      $Data.=chr($suffix%16);
     }
     else
     {
      $Data.=chr(floor($suffix/16));
     };
    $i++;
    };
  };
 $prefix=freadbyte($f);
 $suffix=freadbyte($f);
 $pocetb+=2;
 if($echoit) echo "Prefix: $prefix Suffix: $suffix<BR>";
};

for($x=0;$x<strlen($Data);$x++)
 {
  imagesetpixel($img,$x,$y,$Palette[ord($Data[$x])]);
 };
$Data="";

};

};

 if($biBitCount==24)
{
 $img=imagecreatetruecolor($Width,$Height);
 $Zbytek=$Width%4;

   for($y=$Height-1;$y>=0;$y--)
    {
     for($x=0;$x<$Width;$x++)
      {
       $B=freadbyte($f);
       $G=freadbyte($f);
       $R=freadbyte($f);
       $color=imagecolorexact($img,$R,$G,$B);
       if($color==-1) $color=imagecolorallocate($img,$R,$G,$B);
       imagesetpixel($img,$x,$y,$color);
      }
    for($z=0;$z<$Zbytek;$z++)
     freadbyte($f);
   };
};
return $img;

};

fclose($f);

};

function RGBToHex($Red,$Green,$Blue)
  {
   $hRed=dechex($Red);if(strlen($hRed)==1) $hRed="0$hRed";
   $hGreen=dechex($Green);if(strlen($hGreen)==1) $hGreen="0$hGreen";
   $hBlue=dechex($Blue);if(strlen($hBlue)==1) $hBlue="0$hBlue";
   return($hRed.$hGreen.$hBlue);
  };

function int_to_dword($n)
{
        return chr($n & 255).chr(($n >> 8) & 255).chr(($n >> 16) & 255).chr(($n >> 24) & 255);
}
function int_to_word($n)
{
        return chr($n & 255).chr(($n >> 8) & 255);
}


/*
*------------------------------------------------------------
*                   CUR Image functions
*------------------------------------------------------------
*                      By JPEXS
*/

// Nabbed from: http://jpexs.wz.cz/?page=php&Language=eng

/*
*------------------------------------------------------------
*                    ImageCreateFromCur
*------------------------------------------------------------
*            - Reads image from a CUR file
*
*         Parameters:  $filename - Target cur file to load
*                 $icoColorCount - Cursor color count (For multiple icons ico file)
*                                - 2,16,256, TRUE_COLOR or XP_COLOR
*            Returns: Image ID
*/

function ImageCreateFromCur($filename,$icoColorCount=16,$icoSize=32)
{
$Ikona=GetIconsInfo($filename);

$IconID=-1;

$ColMax=-1;
$SizeMax=-1;

for($p=0;$p<count($Ikona);$p++)
{
$Ikona[$p]["NumberOfColors"]=pow(2,$Ikona[$p]["Info"]["BitsPerPixel"]);
};

for($p=0;$p<count($Ikona);$p++)
{

if(($ColMax==-1)or($Ikona[$p]["NumberOfColors"]>$Ikona[$ColMax]["NumberOfColors"]))
if(($icoSize==$Ikona[$p]["Width"])or($icoSize==-2))
 {
  $ColMax=$p;
 };

if(($SizeMax==-1)or($Ikona[$p]["Width"]>$Ikona[$SizeMax]["Width"]))
if(($icoColorCount==$Ikona[$p]["NumberOfColors"])or($icoColorCount==-2))
 {
   $SizeMax=$p;
 };

if($Ikona[$p]["NumberOfColors"]==$icoColorCount)
if($Ikona[$p]["Width"]==$icoSize)
 {

 $IconID=$p;
 };
};

if($icoSize==-2) $IconID=$SizeMax;
if($icoColorCount==-2) $IconID=$ColMax;

$ColName=$icoColorCount;

if($icoSize==-2) $icoSize="Max";
if($ColName==16777216) $ColName="True";
if($ColName==4294967296) $ColName="XP";
if($ColName==-2) $ColName="Max";
if($IconID==-1) die("Icon with $ColName colors and $icoSize x $icoSize size doesn't exist in this file!");

ReadIcon($filename,$IconID,$Ikona);

 $biBitCount=$Ikona[$IconID]["Info"]["BitsPerPixel"];

  if($Ikona[$IconID]["Info"]["BitsPerPixel"]==0)
  {
  $Ikona[$IconID]["Info"]["BitsPerPixel"]=24;
  };

 $biBitCount=$Ikona[$IconID]["Info"]["BitsPerPixel"];
 if($biBitCount==0) $biBitCount=1;

$Ikona[$IconID]["BitCount"]=$Ikona[$IconID]["Info"]["BitsPerPixel"];

if($Ikona[$IconID]["BitCount"]>=24)
{
$img=imagecreatetruecolor($Ikona[$IconID]["Width"],$Ikona[$IconID]["Height"]);

for($y=0;$y<$Ikona[$IconID]["Height"];$y++)
for($x=0;$x<$Ikona[$IconID]["Width"];$x++)
 {
 $R=$Ikona[$IconID]["Data"][$x][$y]["r"];
 $G=$Ikona[$IconID]["Data"][$x][$y]["g"];
 $B=$Ikona[$IconID]["Data"][$x][$y]["b"];
 $Alpha=round($Ikona[$IconID]["Data"][$x][$y]["alpha"]/2);

 if($Ikona[$IconID]["BitCount"]==32)
 {
 $color=imagecolorexactalpha($img,$R,$G,$B,$Alpha);
 if($color==-1) $color=imagecolorallocatealpha($img,$R,$G,$B,$Alpha);
 }
 else
 {
 $color=imagecolorexact($img,$R,$G,$B);
 if($color==-1) $color=imagecolorallocate($img,$R,$G,$B);
 };

 imagesetpixel($img,$x,$y,$color);

 };

}
else
{
$img=imagecreate($Ikona[$IconID]["Width"],$Ikona[$IconID]["Height"]);
for($p=0;$p<count($Ikona[$IconID]["Paleta"]);$p++)
 $Paleta[$p]=imagecolorallocate($img,$Ikona[$IconID]["Paleta"][$p]["r"],$Ikona[$IconID]["Paleta"][$p]["g"],$Ikona[$IconID]["Paleta"][$p]["b"]);

for($y=0;$y<$Ikona[$IconID]["Height"];$y++)
for($x=0;$x<$Ikona[$IconID]["Width"];$x++)
 {
 imagesetpixel($img,$x,$y,$Paleta[$Ikona[$IconID]["Data"][$x][$y]]);
 };
};

for($y=0;$y<$Ikona[$IconID]["Height"];$y++)
for($x=0;$x<$Ikona[$IconID]["Width"];$x++)
 if($Ikona[$IconID]["Maska"][$x][$y]==1)
  {
   $IsTransparent=true;
   break;
  };
if($Ikona[$IconID]["BitCount"]==32)
{
 imagealphablending($img, FALSE);
 if(function_exists("imagesavealpha"))
  imagesavealpha($img,true);
};

 if($IsTransparent)
 {
  if(($Ikona[$IconID]["BitCount"]>=24)or(imagecolorstotal($img)>=256))
   {
   $img2=imagecreatetruecolor(imagesx($img),imagesy($img));
   imagecopy($img2,$img,0,0,0,0,imagesx($img),imagesy($img));
   imagedestroy($img);
   $img=$img2;
   imagetruecolortopalette($img,true,255);

   };
    $Pruhledna=imagecolorallocate($img,0,0,0);
    for($y=0;$y<$Ikona[$IconID]["Height"];$y++)
     for($x=0;$x<$Ikona[$IconID]["Width"];$x++)
      if($Ikona[$IconID]["Maska"][$x][$y]==1)
       {
        imagesetpixel($img,$x,$y,$Pruhledna);
       };
  imagecolortransparent($img,$Pruhledna);
 };

return $img;

};


$LastIconInfoFile="";
$LastIconInfo="";


/*
*------------------------------------------------------------
*                       ImageCur
*------------------------------------------------------------
*                 - Returns CUR file
*
*         Parameters:       $img - Target Image (Can be more than 1!)
*                      $filename - Target cur file to save
*
*
* Note: For returning cursors to Browser, you have to set header:
*
*             header("Content-type: image/x-cursor");
*
*/

function ImageCur($img,$HotSpotX=0,$HotSpotY=0/*, $img2, $HotSpotX2=0,$HotSpotY2=0, $img3,.... $imgN, $filename (OPTIONAL)*/)
{

$ArgCount=func_num_args();

$ImageCount=ceil(($ArgCount-1)/3);

$filename=func_get_arg($ArgCount-1);

if(is_string($filename))
{
$WriteToFile=true;
}
else
{
 $WriteToFile=false;
 $ImageCount++;
 $filename="";
};

for($p=0;$p<$ImageCount*3;$p+=3)
 {
 if($p<$ArgCount)
 $Image[round($p/3)]=func_get_arg($p);
 if($p+1<$ArgCount)
 $ImageHotSpotX[round($p/3)]=func_get_arg($p+1);
 if($p+2<$ArgCount)
 $ImageHotSpotY[round($p/3)]=func_get_arg($p+2);
 };

$ret="";

$ret.=inttoword(0); //PASSWORD
$ret.=inttoword(2); //SOURCE
$ret.=inttoword($ImageCount); //ICONCOUNT

for($q=0;$q<$ImageCount;$q++)
{
$img=$Image[$q];

$Width=imagesx($img);
$Height=imagesy($img);

$ColorCount=imagecolorstotal($img);

$Transparent=imagecolortransparent($img);
$IsTransparent=$Transparent!=-1;

if($IsTransparent) $ColorCount--;

if($ColorCount==0) {$ColorCount=0; $BitCount=24;};
if(($ColorCount>0)and($ColorCount<=2)) {$ColorCount=2; $BitCount=1;};
if(($ColorCount>2)and($ColorCount<=16)) { $ColorCount=16; $BitCount=4;};
if(($ColorCount>16)and($ColorCount<=256)) { $ColorCount=0; $BitCount=8;};

//ICONINFO:
$ret.=inttobyte($Width);//
$ret.=inttobyte($Height);//
$ret.=inttobyte($ColorCount);//
$ret.=inttobyte(0);//RESERVED

$ret.=inttoword($ImageHotSpotX[$q]);//HotSpot X
$ret.=inttoword($ImageHotSpotY[$q]);//HotSpot Y

$Size=40+($Width/(8/$BitCount)+(($Width/(8/$BitCount))%4))*$Height+(($Width/8+(($Width/8)%4)) * $Height);
if($BitCount<24)
 $Size+=pow(2,$BitCount)*4;
$IconId=1;
$ret.=inttodword($Size); //SIZE
$OffSet=6+16*$ImageCount+$FullSize;
$ret.=inttodword(6+16*$ImageCount+$FullSize);//OFFSET
$FullSize+=$Size;
//-------------

};

for($q=0;$q<$ImageCount;$q++)
{
$img=$Image[$q];
$Width=imagesx($img);
$Height=imagesy($img);
$ColorCount=imagecolorstotal($img);

$Transparent=imagecolortransparent($img);
$IsTransparent=$Transparent!=-1;

if($IsTransparent) $ColorCount--;
if($ColorCount==0) {$ColorCount=0; $BitCount=24;};
if(($ColorCount>0)and($ColorCount<=2)) {$ColorCount=2; $BitCount=1;};
if(($ColorCount>2)and($ColorCount<=16)) { $ColorCount=16; $BitCount=4;};
if(($ColorCount>16)and($ColorCount<=256)) { $ColorCount=0; $BitCount=8;};

//ICONS
$ret.=inttodword(40);//HEADSIZE
$ret.=inttodword($Width);//
$ret.=inttodword(2*$Height);//
$ret.=inttoword(1); //PLANES
$ret.=inttoword($BitCount);   //
$ret.=inttodword(0);//Compress method

$ZbytekMask=(4-($Width/8)%4)%4;

$Zbytek=(4-($Width/(8/$BitCount))%4)%4;
$Size=($Width/(8/$BitCount)+$Zbytek)*$Height+(($Width/8+$ZbytekMask) * $Height);

$ret.=inttodword($Size);//SIZE

$ret.=inttodword(0);//HPIXEL_M
$ret.=inttodword(0);//V_PIXEL_M
$ret.=inttodword($ColorCount); //UCOLORS
$ret.=inttodword(0); //DCOLORS
//---------------

$CC=$ColorCount;
if($CC==0) $CC=256;

if($BitCount<24)
{
 $ColorTotal=imagecolorstotal($img);
 if($IsTransparent) $ColorTotal--;

 for($p=0;$p<$ColorTotal;$p++)
  {
   $color=imagecolorsforindex($img,$p);
   $ret.=inttobyte($color["blue"]);
   $ret.=inttobyte($color["green"]);
   $ret.=inttobyte($color["red"]);
   $ret.=inttobyte(0); //RESERVED
  };

 $CT=$ColorTotal;
 for($p=$ColorTotal;$p<$CC;$p++)
  {
   $ret.=inttobyte(0);
   $ret.=inttobyte(0);
   $ret.=inttobyte(0);
   $ret.=inttobyte(0); //RESERVED
  };
};

if($BitCount<=8)
{

 for($y=$Height-1;$y>=0;$y--)
 {
  $bWrite="";
  for($x=0;$x<$Width;$x++)
   {
   $color=imagecolorat($img,$x,$y);
   if($color==$Transparent)
    $color=imagecolorexact($img,0,0,0);
   if($color==-1) $color=0;
   if($color>pow(2,$BitCount)-1) $color=0;

   $bWrite.=decbinx($color,$BitCount);
   if(strlen($bWrite)==8)
    {
     $ret.=inttobyte(bindec($bWrite));
     $bWrite="";
    };
   };

  if((strlen($bWrite)<8)and(strlen($bWrite)!=0))
    {
     $sl=strlen($bWrite);
     for($t=0;$t<8-$sl;$t++)
      $sl.="0";
     $ret.=inttobyte(bindec($bWrite));
    };
  for($z=0;$z<$Zbytek;$z++)
   $ret.=inttobyte(0);
 };
};

if($BitCount>=24)
{
 for($y=$Height-1;$y>=0;$y--)
 {
  for($x=0;$x<$Width;$x++)
   {
   $color=imagecolorsforindex($img,imagecolorat($img,$x,$y));
   $ret.=inttobyte($color["blue"]);
   $ret.=inttobyte($color["green"]);
   $ret.=inttobyte($color["red"]);
   if($BitCount==32)
    $ret.=inttobyte(0);//Alpha for XP_COLORS
   };
  for($z=0;$z<$Zbytek;$z++)
   $ret.=inttobyte(0);
 };
};

//MASK

 for($y=$Height-1;$y>=0;$y--)
 {
  $byteCount=0;
  $bOut="";
  for($x=0;$x<$Width;$x++)
   {
    if(($Transparent!=-1)and(imagecolorat($img,$x,$y)==$Transparent))
     {
      $bOut.="1";
     }
     else
     {
      $bOut.="0";
     };
   };
  for($p=0;$p<strlen($bOut);$p+=8)
  {
   $byte=bindec(substr($bOut,$p,8));
   $byteCount++;
   $ret.=inttobyte($byte);
  // echo dechex($byte)." ";
  };
 $Zbytek=$byteCount%4;
  for($z=0;$z<$Zbytek;$z++)
   {
   $ret.=inttobyte(0xff);
  // echo "FF ";
   };
 };

//------------------

};//q

if($WriteToFile)
{
 $f=fopen($filename,"w");
 fwrite($f,$ret);
 fclose($f);
}
else
{
 echo $ret;
};

};





$dir = dirname(__FILE__);
$dir = preg_replace('#(.*?)' . $_SERVER['DOCUMENT_ROOT'] . '(.*)#','$2',$dir);

if( $_GET['dir'] )
{
	// use the requested directory.
	$dir .= '/' . $_GET['dir'] . '/';
	$dir = preg_replace('#(.*?)' . $_SERVER['DOCUMENT_ROOT'] . '(.*)#','$2',$dir);
}
// protect against .-based filesystem exploits.
$dir = str_replace('.','',$dir);
$dir = str_replace('//','/','/' . $dir . '/');

$absdir = $_SERVER['DOCUMENT_ROOT'] . $dir;
$absdir = str_replace('//','/',$absdir);

// $dir is something like /images/some_folder/, the absolute dir from the website.
// $absdir is something like /home/username/public_html/images/some_folder, the filesystem absolute dir.
// look in there for a .imgpasswd file.
// this system uses a very simple password format:
// username:password in a file named .imgpasswd (no prefix) in the image folder.
$fpass = @fopen($absdir . '.imgpasswd', 'r');
$cookiename = 'IZSIMAGE' . md5($_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'] . $absdir);

$pass_dir = $absdir;
$pass_dir = dirname($pass_dir);
while(! $fpass && strlen($pass_dir) > strlen(dirname(__FILE__)) )
{
//	echo 'no password file found, trying ' .  $pass_dir . '<br />';
	$fpass = @fopen($pass_dir . '/.imgpasswd', 'r');
	$pass_dir = dirname($pass_dir);
}

$dir = str_replace('//','/',$dir);

if($fpass)
{
	$failed_login = false;
	$logged_in = false;
	$userpass = explode( ':', fread($fpass,filesize($absdir . '.imgpasswd')) );
	if( !array_key_exists(1, $userpass) ) $userpass[] = $userpass[0];
	if( @$_POST['username'] == $userpass[0] && @$_POST['password'] == $userpass[1] )
	{ // they just logged in.  setcookie and let them in
		setcookie($cookiename, md5($userpass[1]), time() + $_COOKIE_TIME, $dir, $_COOKIE_DOMAIN);
		$logged_in = true;
	}
	else
	{
		$logged_in = (@$_COOKIE[$cookiename] === md5($userpass[1]));
		$failed_login = !$logged_in && (@$_POST['username'] || @$_POST['password']);
	}
	fclose($fpass);
}
else
{
	$logged_in = true;
}


function clearthumbs()
{
	global $absdir;
	rmdir_recurse($absdir . 'thumbnails/');
}

function rmdir_recurse($dir)
{
	if(!is_dir($dir)) return;
	$dir = $dir . '/';
	$dir = str_replace('//','/',$dir);
	
	if( $d = opendir($dir) )
	{
		while( ($file = readdir($d)) !== false )
		{
			if(substr($file, 0, 1) == '.') continue;
			echo '<br />Removing ' . $dir . $file;
			if(!is_dir( $dir . $file ) ) 
				unlink($dir . $file);
			else
				rmdir_recurse( $dir . $file);
		}
		closedir($d);
		rmdir($dir);
	}
}

function getthumb($file)
{
	global $dir, $absdir, $_THUMB_WIDTH, $_THUMB_HEIGHT, $_THUMB_PREFIX;
	$thumb = $absdir . 'thumbnails/' . $_THUMB_PREFIX . $file;
	$pubthumb = $dir . 'thumbnails/' . $_THUMB_PREFIX . $file;
	
	$ext = preg_replace('#(.*?)\.(jpe?g|gif|png|bmp|cur|ico)#i','$2',$thumb);
	$ext = strtolower($ext);
	if( $ext == 'jpg' ) $ext = 'jpeg';
//	echo '<br />thumb is: ' . $thumb . '<br />ext is: ' . $ext;
	if(! $ext ) 
	{
		echo 'no ext!';
		return $dir . $file;
	}
	
	// first figure out if we even have to make a thumbnail or not.
	list($w, $h) = getimagesize($absdir . $file);
	if( $w <= $_THUMB_WIDTH[1] && $h <= $_THUMB_HEIGHT[1] && $w >= $_THUMB_WIDTH[0] && $h >= $_THUMB_HEIGHT[0])
	{ // no thumb needed, since the file is small.  just return the file itself.
		return $dir . $file;
	}
	
	if( !is_dir($absdir . 'thumbnails/') )
	{
		mkdir($absdir . 'thumbnails/', 0777);
	}
	
	if( file_exists($thumb) )
	{
		list($nw, $nh) = getimagesize($absdir . 'thumbnails/' . $_THUMB_PREFIX . $file);
		if( $nw && $nh && $nw <= $_THUMB_WIDTH[1] && $nh <= $_THUMB_HEIGHT[1] && 
			$nw >= $_THUMB_WIDTH[0] && $nh >= $_THUMB_HEIGHT[0] ||
			!function_exists('imagecopyresampled') )
		{ // no funkiness needed, since the existing thumb is big/small enough.  return the existing thumb.
			// or, no ability for funkiness, since the imagecopyresampled file is not around.
			return $pubthumb;
		}
		// it's too big or otherwise no good.  unlink it before making a new one.
		@unlink( $thumb );
	}
	
	// get the aspect ratio
	list( $w, $h ) = getimagesize($absdir . $file);
	
	if(!$w) $w = 0.01;
	if(!$h) $h = 0.01;
	$aspect = ($w * 1.0) / ($h * 1.0);
//	echo '<br />aspect for ' . $pubthumb . ': ' . $aspect;
	// figure out the new dims
	if( $w > $h )
	{
		$nw = $_THUMB_WIDTH[1];
		$nh = $nw / $aspect;
	}
	else
	{
		$nh = $_THUMB_HEIGHT[1];
		$nw = $nh * $aspect;
	}
	$nh = ceil($nh);
	$nw = ceil($nw);
	
	if( $nw < $_THUMB_WIDTH[0] ) $nw = $_THUMB_WIDTH[0];
	if( $nh < $_THUMB_HEIGHT[0] ) $nh = $_THUMB_HEIGHT[1];
	
	$function = 'imagecreatefrom' . $ext;
	if(function_exists($function))
	{
		//echo '<br />calling ' . $function;
		$src = $function($absdir . $file);
	}
	else
	{
		return $dir . $file;
	}
	//echo '<br />1 Dimensions for ' . $pubthumb . ': ' . $nw . 'w ' . $nh . 'h';
	
	$dst = imagecreatetruecolor( $nw, $nh );
	
	imagecopyresampled($dst, $src, 0, 0, 0, 0, $nw, $nh, $w, $h);
	
	$function = 'image' . $ext;
	if(function_exists($function))
	{
//		echo '<br />calling ' . $function;
		$function( $dst, $thumb );
	}
	else
	{
		$pubthumb = $dir . $file;
	}
	
	imagedestroy($src);
	imagedestroy($dst);
	
//	list($a,$b) = getimagesize($thumb);
//	echo '<br />5 Actual Dimensions for ' . $pubthumb . ': ' . $a . 'w ' . $b . 'h';
	
//	echo '<br />returning:' . $pubthumb;
	return $pubthumb;
}

function navbar($total, $pgnum)
{
	global $dir, $_PAGINATION_FACTOR;
	$total = ceil (1.0 * $total / $_PAGINATION_FACTOR);
	$navstring = '<div class="navbar"><div class="navbarleft">Page: &nbsp; &nbsp; ';
	if ($pgnum > 1)
		$navstring .= '<a href="' . $dir . '?page=' . urlencode($pgnum - 1) . '#images" title="Previous page">&laquo; Previous</a> &nbsp; &nbsp; ';
	else $navstring .= '&nbsp;';
	$navstring .= '</div><div class="navbarright">';
	if ($pgnum < $total)
		$navstring .= '&nbsp; &nbsp; <a href="' . $dir . '?page=' . urlencode($pgnum + 1) . '#images" title="Next page">Next &raquo;</a>';
	else $navstring .= "&nbsp;";
	$navstring .= '</div><div class="navbarcenter">';
	for ($i = 1; $i <= $total; ++ $i)
	{
		if ($i !== $pgnum)
			$navstring .= '<a href="' . $dir . '?page=' . urlencode($i) . '#images" title="Page ' . $i . '"> &nbsp; ' . $i . ' &nbsp; </a> ';
		else
			$navstring .= ' &nbsp; ' . $i . ' &nbsp; ';
	}
	return $navstring . '</div></div>';
}

function is_image($filename)
{
	$filename = strtolower ($filename);
	return strpos($filename,'.jpeg')!==false || strpos($filename, '.jpg')!==false || strpos($filename,'.png')!==false || strpos($filename,'.gif')!==false || strpos($filename,'.bmp')!==false;
}

function format_size($size)
{
	$suffix = '';
	$cooked = 0.0;
	if ($size <= (1 << 9))
	{
		$suffix = 'B';
		$cooked = $size;
	}
	elseif ($size <= (1 << 19))
	{
		$suffix = 'kiB';
		$cooked = $size / 1024.0;
	}
	elseif ($size <= (1 << 29))
	{
		$suffix = 'MiB';
		$cooked = $size / 1048576.0;
	}
	elseif ($size <= (1 << 39))
	{
		$suffix = 'GiB';
		$cooked = $size / 1073741824.0;
	}
	elseif ($size <= (1 << 49))
	{
		$suffix = 'PiB';
		$cooked = $size / 1099511627776.0;
	}
	elseif ($size <= (1 << 59))
	{
		$suffix = 'EiB';
		$cooked = $size / 1125899906842624.0;
	}
	elseif ($size <= (1 << 69))
	{
		$suffix = 'ZiB';
		$cooked = $size / 1152921504606846976.0;
	}
	elseif ($size <= (1 << 79))
	{
		$suffix = 'YiB';
		$cooked = $size / 1180591620717411303424.0;
	}
	else
	{
		$suffix = (intval (log10 ($size)) / 3) * 3;
		$cooked = $size / pow (10, suffix);
		$suffix = 'E+' . $suffix;
	}
	$cooked = number_format ($cooked, 1);
	return $cooked . '</td><td class="listunit">' . $suffix;
}

$dir = str_replace('//','/',$dir);

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title><?php echo $dir ?> - IZSImage Gallery</title>
<meta name="generator" content="IZSImage Gallery" />
<link rel="creator" href="http://isaacschlueter.com/" title="Isaac Z. Schlueter" />
<style type="text/css">
a img { border:0 }
html {
  font-family:Trebuchet MS, Trebuchet, Verdana, Tahoma, sans-serif;
  background:#ccc;
}
body {
  background:#fff;
  border:5px solid #eee;
  margin:1em;
  padding:1em;
}
h1 { margin-top:0;padding-top:0; }
.foot {
	font-size:0.8em;
	font-style:italic;
	text-align:right;
	clear: both;
}
.warning {
	border:4px solid #900;
	padding:1em;
}
.thumbbox {
	float: left;
	width: 150px;
	height: 168px;
	font-family: tahoma, verdana, dejavu sans, bitstream vera sans, sans-serif;
	font-size: 9px;
	color: black;
	background-color: white;
	text-align: center;
	display: table;
	vertical-align: middle;
	text-align: center;
	padding: 0px;
	margin: 2px; 
}
.innerbox {
	height: 150px;
	line-height: 150px;
	top: 0px;
	float: none;
	clear: both;
	width: 150px;
	display: table-cell;
	vertical-align: middle;
	background-color: #E0E0E0;
	padding: 0px;
	margin: 0px;
}
.underbox {
	height: 16px;
	top: 150px;
	float: none;
	text-align: center;
	display: table-cell;
	vertical-align: middle;
	padding: 0px;
	margin: 0px;
}
.container {
	display: table-row;
	padding: 0px;
	margin: 0px;
}
.navbar {
	clear: both;
	padding: 2em 0px 2em 0px;
}
.navbarleft {
	float: left;
	text-align: left;
	width: 10em;
}
.navbarright {
	float: right;
	text-align: right;
	width: 10em;
}
.navbarcenter {
	margin: 0px auto;
	text-align: center;
}
table {
	border: 0px none;
	padding-top: 3em;
}

th {
	border-bottom: 2px solid black;
	padding: 0px 0.5em 0px 0.5em;
}

td {
	border: 0px none;
	padding: 0px 0.5em 0px 0.5em;
}

.listname {
	text-align: left;
}

.listdate, .listsize, .listunit {
	text-align: right;
}

td.listdate, td.listsize, td.listunit {
	font-family: monospace;
}

img {
	border: 0px;
	vertical-align: middle;
}
a {
	text-decoration: none;
}
</style>
</head>
<body>

<h1>Index of <?php echo $dir ?></h1>

<?php

if( basename($dir) == 'thumbnails' )
{
	?><p>This is a thumbnail directory.  It has nothing that you want.  Please
		<a href="<?php echo dirname($dir) ?>">go back up a level</a> to view pictures.</p>
	</body></html>
	<?php
	die();
}

if( $_CLEARTHUMBS_PASSWORD == 'your_password_here' || !$_CLEARTHUMBS_PASSWORD)
{
	?><div class="warning">
	<p>For security reasons, you need to change the <code>$_CLEARTHUMBS_PASSWORD</code> variable
	to something other than the default before you can use the IZSImage Gallery script.  
	Otherwise, bad types can arbitrarily reset all your	thumbnails repeatedly, causing undue stress on the 
	server.</p>
	<p>As a general rule, you must always assume that everyone is always out to get you when dealing on the
	internet.  It's a sad but true fact that the internet is the biggest computer network in the world, and
	it is home to some of the most devious and evil people you'll ever find.</p>
	<p>Open up gallery.php, and read through the instructions.  Make sure that you change the <code>$_CLEARTHUMBS_PASSWORD</code>
	to something hard to guess.</p>
	<p>This password is used if you want to delete all the files in the thumbnail directory and start fresh.
	(It's very useful if you change the thumbnail prefix or if you somehow get some corrupted files in there.)</p>
	</div></body></html>
	<?php
	die();
}
if( $logged_in )
{
	$f = array();
	$d = array();
	$ni = array();
	 // echo 'looking in ' . $absdir . ' for files....<br />';
	if( is_dir($absdir) )
	{
		if( $dirhandle = opendir($absdir) )
		{
			while( ($file = readdir($dirhandle)) !== false)
			{
			  if( substr( $file, 0, 1 ) == '.' && $file !== '..' || $file == basename(__FILE__) ||
			  	$file == 'thumbnails' || $file == 'error_log' ) continue;

			  if( is_dir( $absdir . $file ) ) $d[] = $file . '/';
			  else if( !is_image ( $file ) ) $ni[] = $file;
			  else $f[] = $file;
			}
			closedir($dirhandle);
		}
	}
	if(@$_GET['clearthumbs'] === $_CLEARTHUMBS_PASSWORD) clearthumbs();

	$g = array();
	$imgs = array();

	sort($d);
	sort($ni);
	sort($f);
	$pgnum = $_GET['page'] ? intval( $_GET['page'] ) : 1;
	$navstring = navbar( count( $f ), $pgnum );
	$f = array_merge( $d, $ni, array_slice( $f, $_PAGINATION_FACTOR * ( $pgnum - 1 ), $_PAGINATION_FACTOR ) );

	foreach($f as $k => $v)
	{
		$i = '';
		if(is_image($v))
		{
			$i = '<a href="' . $dir . $v . '" title="View ' . $v . ' (';
			list($w, $h) = getimagesize($absdir . $v);
			$s = filesize($absdir . $v);
			$i .= $w . 'px by ' . $h . 'px, ' . $s . ' bytes)"><div class="thumbbox"><div class="container"><div class="innerbox"><img src="';
			
			$i .= getthumb($v);
			if (preg_match("/.{25}/", $v))
			{
				$truncFile = preg_replace("/^(.{1,18}).*(.{7})$/", "$1...$2", $v);
			}
			else
			{
				$truncFile = $v;
			}
			$i .=  '" alt="Thumbnail of ' . $v . '" /></div></div><div class="container"><div class="underbox">' . $truncFile . '</div></div></div></a>';
			
			$imgs[] = $i;
		}
		else
		{
			$g[] = $v;
		}
	}

	echo '<div>&larr; <a href="' . dirname($dir) . '">Up to ' . basename(dirname($dir)) . '/</a></p><div style="clear: both">';

	if( ! empty($imgs) )
	{
		echo '<a name="images" />' . $navstring . '<p class="imgs">' . implode("\n",$imgs) . '</p>' . $navstring;
	}

	$g = array_filter ($g, create_function ('$filename', 'return $filename !== "../";'));
	if(! empty($g) )
	{
		if( $uptodir == '' || $uptodir == '/' ) $uptodir = 'root';
		echo '<a name="files" /><table><tr><th>&nbsp;</th><th class="listname">Name</th><th class="listdate">Modified</th><th colspan="2" class="listsize">Size</th></tr>';
		
		foreach($g as $k => $v)
		{
			$finfo = stat( $absdir . $v );
			if( is_dir( $absdir . $v ) ) echo '<tr><td>&raquo;</td><td class="listname"><a href="' . $dir . $v . '">' . $v . '</a></td><td class="listdate">' . date('d-M-Y', $finfo['mtime']) . '</td><td class="listsize">&nbsp;</td><td class="listunit">&nbsp;</td></tr>';
			else echo '<td>&nbsp;</td><td class="listname"><a href="' . $dir . $v . '">' . $v . '</a></td><td class="listdate">' . date('d-M-Y', $finfo['mtime']) . '</td><td class="listsize">' . format_size($finfo['size']) . '</td></tr>';
		}
		echo '</table>' . "\n";
	}
	echo '</div>';
}
else
{
	if( $failed_login ) echo '<p class="warning">Wrong username and/or password!  Please try again.</p>';
	else echo '<p class="warning">You\'ll have to log in to see this set of pics.</p>';
	if( $_CONTACT_URL )
	{
		?><p>If you don't know what the code is, you can <a href="<?php echo $_CONTACT_URL ?>">ask for it</a>, and I may share it.</p>
		<?php
	}
	?>
	<form action="<?php echo $dir ?>" method="post" id="izsimage_login" name="izsimage_login">
		<fieldset>
			<label for="username">Username:</label> <input type="text" size="12" maxlength="12" id="username" name="username" value="" /><br />
			<label for="password">Password:</label> <input type="password" size="12" maxlength="12" id="password" name="password" value="" /><br />
			<input type="submit" value="Go!" />
			<input type="reset" value="Reset!" />
		</fieldset>
	</form>
	<?php
}

?>
<p class="foot">Powered by <a href="http://isaacschlueter.com/plugins/i-made/image-gallery/">IZSImage Gallery</a> derivative, source code <a href="http://www.gurdasani.com/gallery.txt">available</a>.  
	All images &copy; Copyright.  All Rights Reserved.</p>
</body></html>
